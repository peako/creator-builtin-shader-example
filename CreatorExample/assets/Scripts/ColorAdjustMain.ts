import { _decorator, Component, Node } from 'cc';
import { ColorAdjustBase } from './ColorAdjustBase';
const { ccclass, property } = _decorator;

@ccclass('ColorAdjustMain')
export class ColorAdjustMain extends Component {
    private m_Timer: number = 0;
    private m_Ratio: number = 0;

    private setRatio(ratio: number) {
        this.m_Ratio = ratio;
        const adjuster = ColorAdjustBase.adjuster;
        adjuster.forEach(adjust => {
            adjust.updateTimer(ratio);
        });
    }

    private lerp(a: number, b: number, r: number) {
        return a + (b - a) * r;
    }

    update(deltaTime: number) {
        const ratio = Math.abs(Math.sin(this.m_Timer));
        this.setRatio(ratio);
        this.m_Timer += (deltaTime * (this.lerp(0.1, 0.8, ratio)));
    }
}

